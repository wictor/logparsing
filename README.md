LogParser skeleton
==================

Simple console applications log parser - skeleton, to be customized and enhanced for a specific purpose.


License
-------
Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).