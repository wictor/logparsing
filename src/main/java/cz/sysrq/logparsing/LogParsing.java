package cz.sysrq.logparsing;

import cz.sysrq.logparsing.parser.LogParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class LogParsing {

    private static final Logger logger = LoggerFactory.getLogger(LogParsing.class);

    public static void main(String[] args) {
        logger.info("LogParsing params {} (cnt={})", args, args.length);

        if (args.length != 1) {
            logger.error("Please run: LogParsing [input log file]");
            System.exit(1);
        }

//        logger.info("LogParsing arg: {}", args[0]);
//        if (args[0] == null || args[0].isEmpty()) {
//
//        }

        LogParser parser = new LogParser();
        parser.addLogRecordHandler(System.out::println);
        parser.parseLog(new File(args[0]));

    }





}
