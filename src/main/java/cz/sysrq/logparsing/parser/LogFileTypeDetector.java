package cz.sysrq.logparsing.parser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.spi.FileTypeDetector;

/**
 * Own {@link FileTypeDetector} to make reliable distinguish between plain text (utf-8) and gzip files.
 * <p/>
 * This detector uses file extension + tries to read a bit of the file as a text file.
 * <p/>
 * A detector can be registered to runtime - see https://odoepner.wordpress.com/2013/07/29/transparently-improve-java-7-mime-type-recognition-with-apache-tika/
 * <p/>
 * <em>To register the implementation with the Java Service Provider Interface (SPI), you need to have a plaintext
 * file /META-INF/services/java.nio.file.spi.FileTypeDetector in the same jar that contains the class LogFileTypeDetector.
 * The text file contains just one line with the fully qualified name of the implementing class:</em>
 * <code>
 * cz.sysrq.logparsing.parser.LogFileTypeDetector
 * </code>
 * <em>With Maven, you simply create the file src/main/resources/META-INF/services/java.nio.file.spi.FileTypeDetector
 * containing the line shown above. See the ServiceLoader documentation for details about Java SPI.</em>
 */
public class LogFileTypeDetector extends FileTypeDetector {

    private static final Logger logger = LoggerFactory.getLogger(LogFileTypeDetector.class);

    @Override
    public String probeContentType(Path path) throws IOException {
        logger.debug("Probing content of \"{}\"", path);
        // 1. check file extension
        String pathStr = path.toString().toLowerCase();
        if (pathStr.endsWith(".gz")) {
            // -> gz -> "gzip"
            logger.debug("GZip file extension found.");
            return "application/gzip";
        } else if (pathStr.endsWith(".log") | pathStr.endsWith(".txt")) {
            // -> log/txt -> try content
            logger.debug("Text file extension found.");
            return getTypeByBytesExam(path);
        } else {
            // -> anything else -> try content
            logger.debug("Unknown file extension found.");
            return getTypeByBytesExam(path);
        }
    }

    /**
     * Read the first n bytes and try to read it as UTF-8.
     *
     * @param path file path to examination
     * @return text MIME type if the bytes are UTF-8 compatible or {@code null}
     * @throws IOException when reading fails
     */
    private String getTypeByBytesExam(Path path) throws IOException {
        try (DataInputStream dis = new DataInputStream(new FileInputStream(path.toFile()))) {
            byte[] b = new byte[1000];
            int readBytes = dis.read(b);
            logger.debug("Read the first {} bytes for examination", readBytes);
            if (readBytes > 0 && isValidUTF8(b)) {
                logger.debug("UTF-8 text compatible found");
                return "text/plain";
            }
        }
        logger.debug("Unknown content");
        return null;
    }

    /**
     * Checks if given byte array is valid UTF-8 encoded.
     *
     * @param bytes byte array to be checked
     * @return true when valid UTF8 encoded
     */
    private boolean isValidUTF8(final byte[] bytes) {
        try {
            Charset.availableCharsets().get("UTF-8").newDecoder().decode(ByteBuffer.wrap(bytes));
        } catch (CharacterCodingException e) {
            return false;
        }
        return true;
    }
}
