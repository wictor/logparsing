package cz.sysrq.logparsing.parser;

import cz.sysrq.logparsing.model.LogItemMeta;
import cz.sysrq.logparsing.model.LogRecord;
import cz.sysrq.logparsing.model.LogRecordFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.spi.FileTypeDetector;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

/**
 * General log parser.
 */
public class LogParser {

    private static final Logger logger = LoggerFactory.getLogger(LogParser.class);

    private static final String LOG_START_WITH = "20";
    private static final String LOG_PTN_DATETIME = "20[0-9]{2}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]{3}";
    private static final String LOG_PTN_LEVEL = "[A-Z]+";
    private static final String LOG_PTN_THREAD = "[^]]+";
    private static final String LOG_PTN_LOGGER = "[a-z.]*\\w+";

    static final String MIME_TEXT = "text/plain";
    static final String MIME_GZIP = "application/gzip";

    private final String logStartWith;

    private final FileTypeDetector fileTypeDetector;

    private final List<LogRecordHandler> logRecordHandlers;

    /**
     * "2019-09-24 11:49:21,867 INFO  [pool-4-thread-2] MessageProcessor: ..."
     * "2019-09-24 11:49:22,386 INFO  [ExecuteThread: '0' for queue: 'default'] MessageProcessor: "
     * (group1                ) (gr2)  (group3        ) (group4):
     * <p>
     * https://regexr.com/
     */
    private final Pattern logStartPtn;

    /**
     * Default values.
     */
    public LogParser() {
        logStartWith = LOG_START_WITH;
        logStartPtn = Pattern.compile("^(" + LOG_PTN_DATETIME + ")\\s+(" + LOG_PTN_LEVEL + ")\\s+\\[(" +
                LOG_PTN_THREAD + ")]\\s+(" + LOG_PTN_LOGGER + "):");

        fileTypeDetector = new LogFileTypeDetector();
        logRecordHandlers = new LinkedList<>();
    }

    /**
     * Add {@link LogRecordHandler} to process each parsed log record.
     *
     * @param handler log record handler
     */
    public void addLogRecordHandler(LogRecordHandler handler) {
        logRecordHandlers.add(handler);
    }

    /**
     * Parses the provided log file (plain text or GZip-compressed log file supported).
     *
     * @param file log file
     */
    public void parseLog(File file) {
        try {
            String contentType = fileTypeDetector.probeContentType(file.toPath());
            if (contentType == null) {
                throw new IllegalArgumentException("Content type cannot be determined.");
            }
            logger.info("Detected MIME type: {}", contentType);
            switch (contentType) {
                case MIME_TEXT:
                    parseTextLog(file);
                    break;
                case MIME_GZIP:
                    parseGzLog(file);
                    break;
                default:
                    throw new UnsupportedOperationException("Unsupported file content type: MIME type = " + contentType);
            }
        } catch (IOException e) {
            logger.error("IO exception", e);
        }
    }

    /**
     * Parses explicitly GZip-compressed log file.
     *
     * @param gzFile log file
     */
    public void parseGzLog(File gzFile) {
        try (GZIPInputStream gzipInputStream = new GZIPInputStream(new FileInputStream(gzFile))) {
            parseInputStream(gzipInputStream);
        } catch (FileNotFoundException e) {
            logger.error("File not found", e);
        } catch (IOException e) {
            logger.error("IO exception", e);
        }
    }

    /**
     * Parses explicitly plain text log file.
     *
     * @param txtFile log file
     */
    public void parseTextLog(File txtFile) {
        try (FileInputStream inputStream = new FileInputStream(txtFile)) {
            parseInputStream(inputStream);
        } catch (FileNotFoundException e) {
            logger.error("File not found", e);
        } catch (IOException e) {
            logger.error("IO exception", e);
        }
    }

    private void parseInputStream(InputStream inputStream) {
        long lines = 0;
        long logItems = 0;
        try (BufferedReader in = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            LogItemMeta meta = null, newMeta;
            List<String> logItemLines = new LinkedList<>();
            // first line
            if ((line = in.readLine()) != null) {
                lines++;
                meta = parseNewLogItemMeta(line);
                Objects.requireNonNull(meta, "Log record not found at the file beginning");
                logItemLines.add(line);
            }
            // the rest
            while ((line = in.readLine()) != null) {
                lines++;
                if (mayBeNewLogItem(line) && (newMeta = parseNewLogItemMeta(line)) != null) {
                    // submit a new LogItem
                    logItems++;
                    submit(meta, logItemLines);
                    logItemLines.clear();
                    meta = newMeta;
                }
                logItemLines.add(line);
            }
            // submit the last event (there should be at least one log item in the file)
            logItems++;
            submit(meta, logItemLines);
        } catch (IOException e) {
            logger.error("IO exception", e);
        } finally {
            logger.info("Read finished: {} log items on {} lines", logItems, lines);
        }
    }

    /**
     * Quick test before applying RE.
     *
     * @param line a log line
     * @return {@code true} when the line may be a new log line
     */
    boolean mayBeNewLogItem(String line) {
        return line.startsWith(logStartWith);
    }

    LogItemMeta parseNewLogItemMeta(String line) {
        Matcher m = logStartPtn.matcher(line);
        if (m.find()) {
            String dateTimeStr = m.group(1);
            String levelStr = m.group(2);
            String threadStr = m.group(3);
            String loggerStr = m.group(4);
            LocalDateTime ldt = LocalDateTime.parse(dateTimeStr, LogItemMeta.LOG_DATE_TIME_PTN);
            LogItemMeta.ELogLevel lvl = LogItemMeta.ELogLevel.valueOf(levelStr);
            return new LogItemMeta(ldt, lvl, threadStr, loggerStr);
        }
        return null;
    }

    private void submit(LogItemMeta meta, List<String> logItemLines) {
        LogRecord logRecord = LogRecordFactory.make(meta, logItemLines);
        // TODO VSV: implement errorHandler (implements LogErrorHandler) call (logRecord implements LogError)
        // TODO VSV: implement timedActionHandler (implements LogTimedActionHandler) call (logRecord implements LogTimedAction)

        // General log record handler
        for (LogRecordHandler handler : logRecordHandlers) {
            handler.onLogRecord(logRecord);
        }
    }

}
