package cz.sysrq.logparsing.parser;

import cz.sysrq.logparsing.model.LogRecord;

/**
 * LogRecord processing handler interface.
 */
public interface LogRecordHandler {

    /**
     * Callback method called on every parsed log record.
     *
     * @param logRecord log record
     */
    void onLogRecord(LogRecord logRecord);
}
