package cz.sysrq.logparsing.model;

import java.util.List;
import java.util.Objects;

/**
 * {@link LogRecord} factory.
 */
public class LogRecordFactory {

    public static LogRecord make(LogItemMeta meta, List<String> logItemLines) {
        // Validate required parameters: at least one line of log with meta info
        Objects.requireNonNull(meta);
        Objects.requireNonNull(logItemLines);
        Objects.requireNonNull(logItemLines.get(0));
        return new GenericLogRecord(meta, logItemLines);
    }
}
