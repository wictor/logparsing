package cz.sysrq.logparsing.model;

import java.util.List;

/**
 * Log record base interface.
 *
 * Log record is a single line or multiline record created by a single logger call.
 */
public interface LogRecord {

    /**
     * @return metadata such as datetime, log level, thread..
     */
    LogItemMeta getMetadata();

    /**
     * @return log record data (without the metadata), single element list for a one-liners
     */
    List<String> getLines();

}
