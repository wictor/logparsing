package cz.sysrq.logparsing.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Basic log item metadata.
 * <p>
 * <code>
 * 2019-09-24 11:49:21,867 INFO  [pool-4-thread-2] MessageProcessor: ...
 * </code>
 */
public class LogItemMeta {

    public static final DateTimeFormatter LOG_DATE_TIME_PTN = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss,SSS");

    public enum ELogLevel {TRACE, DEBUG, INFO, WARN, ERROR, FATAL}

    private final LocalDateTime dateTime;
    private final ELogLevel level;
    private final String threadName;
    private final String loggerName;

    public LogItemMeta(LocalDateTime dateTime, ELogLevel level, String threadName, String loggerName) {
        this.dateTime = dateTime;
        this.level = level;
        this.threadName = threadName;
        this.loggerName = loggerName;
    }

    /**
     * Format "yyyy-MM-dd HH:mm:ss,SSS LEVEL [thread] loggerName"
     *
     * @return String like the log line
     */
    public String normalized() {
        String dateTimeStr = dateTime.format(LOG_DATE_TIME_PTN);
        return String.format("%s %s [%s] %s:", dateTimeStr, level.name(), threadName, loggerName);
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public ELogLevel getLevel() {
        return level;
    }

    public String getThreadName() {
        return threadName;
    }

    public String getLoggerName() {
        return loggerName;
    }

    @Override
    public String toString() {
        return "LogItemMeta{" + normalized() + "}";
    }
}
