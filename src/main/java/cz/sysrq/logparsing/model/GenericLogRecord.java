package cz.sysrq.logparsing.model;

import java.util.List;

/**
 * {@link LogRecord} base generic class.
 */
class GenericLogRecord implements LogRecord {

    private final LogItemMeta meta;
    private final List<String> lines;

    GenericLogRecord(LogItemMeta meta, List<String> lines) {
        this.meta = meta;
        this.lines = lines;
    }

    @Override
    public LogItemMeta getMetadata() {
        return meta;
    }

    @Override
    public List<String> getLines() {
        return lines;
    }

    @Override
    public String toString() {
        String truncatedLines = lines.size() > 1 ? String.format("%s ...(%d lines truncated)", lines.get(0), lines.size() - 1) : lines.get(0);
        return "GenericLogRecord{" +
                "meta=" + meta.normalized() +
                ", lines=" + truncatedLines +
                '}';
    }
}
