package cz.sysrq.logparsing.parser;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.Assert.*;


/**
 * File content type guessing tests.
 */
public class FileContentTypeTest {

    private File plainText;
    private File gzip;

    @Before
    public void setUp() throws Exception {
        plainText = new File(this.getClass().getResource("/mime-plaintext.log").toURI());
        gzip = new File(this.getClass().getResource("/mime-gz.log.gz").toURI());
    }

    @Test
    public void testPlainTextGuessing_OwnFileTypeDetector() throws IOException {
        assertEquals(LogParser.MIME_TEXT, guessTypeByOwnFileTypeDetector(plainText));
    }

    @Test
    public void testGzipGuessing_OwnFileTypeDetector() throws IOException {
        assertEquals(LogParser.MIME_GZIP, guessTypeByOwnFileTypeDetector(gzip));
    }

    @Test @Ignore
    public void testPlainTextGuessing_FileTypeDetector() throws IOException {
        // Default FileTypeDetector (JDK8) found it on my Win10 PC as unknown (null value returned)
        // GitLab throws AssertionError: expected null, but was:<text/x-log>
        assertNull(guessTypeByFileTypeDetector(plainText));
    }

    @Test @Ignore
    public void testGzipGuessing_FileTypeDetector() throws IOException {
        // Default FileTypeDetector (JDK8) found it on my Win10 PC as "application/x-gzip"
        // GitLab throws: ComparisonFailure: expected:<application/[x-]gzip> but was:<application/[]gzip>
        assertEquals("application/x-gzip", guessTypeByFileTypeDetector(gzip));
    }

    @Test
    public void testPlainTextGuessing_URLConnection() throws IOException {
        // URLConnection found it on my Win10 PC as "content/unknown"
        assertEquals("content/unknown", guessTypeByConnection(plainText));
    }

    @Test
    public void testGzipGuessing_URLConnection() throws IOException {
        // URLConnection found it on my Win10 PC as "application/octet-stream"
        assertEquals("application/octet-stream", guessTypeByConnection(gzip));
    }

    /**
     * Use custom {@link LogFileTypeDetector} FileTypeDetector.
     *
     * @param file file path to examine
     * @return MIME type of the known type or {@code null}
     * @throws IOException when an error occurs reading the file
     */
    private String guessTypeByOwnFileTypeDetector(File file) throws IOException {
        return new LogFileTypeDetector().probeContentType(filePath(file));
    }

    private String guessTypeByFileTypeDetector(File file) throws IOException {
        return Files.probeContentType(filePath(file));
    }

    private Path filePath(File file) {
        Path path = file.getAbsoluteFile().toPath();
        System.out.println(path);
        return path;
    }

    private String guessTypeByConnection(File file) throws IOException {
        URLConnection connection = file.toURI().toURL().openConnection();
        System.out.println(connection);
        return connection.getContentType();
    }
}
