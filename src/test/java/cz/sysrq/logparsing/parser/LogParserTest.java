package cz.sysrq.logparsing.parser;

import cz.sysrq.logparsing.model.LogItemMeta;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LogParserTest {

    private LogParser lp;

    @Before
    public void setUp() {
        lp = new LogParser();
    }

    @Test
    public void testMayBeNewLogItem() {
        assertTrue(lp.mayBeNewLogItem("2019-09-24 00:00:03,065 INFO  [pool-4-thread-1] KafkaProcessor: Successfully transferred 2 items from Kafka in 115 ms, msgId=a1e190-df45-4f0c-9d3b-7d4519827 f2edca-0a15-499b-a434-7997d5422"));
        assertTrue(lp.mayBeNewLogItem("2019-09-24 11:49:22,386 INFO  [ExecuteThread: '0' for queue: 'default'] MessageProcessor: ojfoi jf"));
        assertFalse(lp.mayBeNewLogItem("blahBlah 2019-09-24 00:00:03,065 INFO  [pool-4-thread-1] KafkaProcessor: Successfully"));
        assertFalse(lp.mayBeNewLogItem("   blahBlah 2019-09-24 00:00:03,065 INFO  [pool-4-thread-1] KafkaProcessor: Successfully"));
    }

    @Test
    public void testParseNewLogItemMeta() {
        assertNull(lp.parseNewLogItemMeta("2000-00-00 00>blah 2019-09-24 00:00:03,065 INFO  [pool-4-thread-1] KafkaProcessor: Successfully transferred 2 items from Kafka in 115 ms, msgId=a1e190-df45-4f0c-9d3b-7d4519827 f2edca-0a15-499b-a434-7997d5422"));

        LogItemMeta meta;

        meta = lp.parseNewLogItemMeta("2019-09-24 00:00:03,065 INFO  [pool-4-thread-1] KafkaProcessor: Successfully transferred 2 items from Kafka in 115 ms, msgId=a1e190-df45-4f0c-9d3b-7d4519827 f2edca-0a15-499b-a434-7997d5422");
        assertNotNull(meta);
        assertEquals("pool-4-thread-1", meta.getThreadName());
        assertEquals("2019-09-24 00:00:03,065 INFO [pool-4-thread-1] KafkaProcessor:", meta.normalized());

        meta = lp.parseNewLogItemMeta("2019-09-24 00:00:03,065 INFO  [pool-4-thread-1] o.c.b.KafkaProcessor: Successfully transferred 2 items from Kafka in 115 ms, msgId=a1e190-df45-4f0c-9d3b-7d4519827 f2edca-0a15-499b-a434-7997d5422");
        assertNotNull(meta);
        assertEquals("o.c.b.KafkaProcessor", meta.getLoggerName());
        assertEquals("2019-09-24 00:00:03,065 INFO [pool-4-thread-1] o.c.b.KafkaProcessor:", meta.normalized());
    }


}